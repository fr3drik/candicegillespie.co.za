﻿// include plug-ins
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var del = require('del');

var config = {
  //Include all js files but exclude any min.js files
  jquerySrc: [
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/jquery-validation/dist/jquery.validate.min.js',
        'bower_components/jquery-validation-unobtrusive/jquery.validate.unobtrusive.min.js'
  ],
  bootstrapsrc: [
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/respond-minmax/dest/respond.min.js'
  ],
  modernizrsrc: ['bower_components/modernizr/modernizr.js'],
  showdownsrc: ['bower_components/showdown/dist/showdown.min.js'],
  bootstrapcss: ['bower_components/bootstrap/dist/css/bootstrap.css'],
  bootstrapfonts: 'bower_components/bootstrap/dist/fonts/*.*'
}

//clean
gulp.task('cleanjquery', function () {
  return del(['Scripts/jquery-bundle.min.js']);
});
gulp.task('cleanbootstrap', function () {
  return del(['Scripts/bootstrap-bundle.min.js']);
});
gulp.task('cleanmodernizer', function () {
  return del(['Scripts/modernizer.min.js']);
});
gulp.task('cleanshowdown', function () {
  return del(['Scripts/showdown.min.js']);
});
gulp.task('cleancss', function () {
  return del(['Content/dist/bootstrap.min.css']);
});
gulp.task('cleanfonts', function () {
  return del(['Content/dist/fonts/*.*']);
});

//tasks
gulp.task('jqueryscripts', ['cleanjquery'], function () {

  return gulp.src(config.jquerySrc)
    .pipe(uglify())
    .pipe(concat('jquery-bundle.min.js'))
    .pipe(gulp.dest('Scripts/'));
});
gulp.task('bootstrapscripts', ['cleanbootstrap'], function () {
  return gulp.src(config.bootstrapsrc)
    .pipe(uglify())
    .pipe(concat('bootstrap-bundle.min.js'))
    .pipe(gulp.dest('Scripts/'));
});
gulp.task('modernizerscripts', ['cleanmodernizer'], function () {
  return gulp.src(config.bootstrapsrc)
    .pipe(uglify())
    .pipe(concat('modernizer.min.js'))
    .pipe(gulp.dest('Scripts/'));
});
gulp.task('showdownscripts', ['cleanshowdown'], function () {
  return gulp.src(config.showdownsrc)
    .pipe(uglify())
    .pipe(concat('showdown.min.js'))
    .pipe(gulp.dest('Scripts/'));
});
gulp.task('css', ['cleancss'], function () {
  return gulp.src(config.bootstrapcss)
    //.pipe(uglify())
    .pipe(concat('bootstrap.min.css'))
    .pipe(gulp.dest('Content/dist/css/'));
});
gulp.task('fonts', ['cleanfonts'], function () {
  return gulp.src(config.bootstrapfonts)
    .pipe(gulp.dest('Content/dist/fonts/'));
});

//Set a default tasks
gulp.task('default', ['jqueryscripts', 'bootstrapscripts', 'modernizerscripts', 'showdownscripts', 'css', 'fonts'], function () { });